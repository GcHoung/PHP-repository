<?php

require "./Pagination/page.class.php";

$sql = '';
$page = new Pagination\Page($sql,10, 2,false);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>demo</title>
    <link rel="stylesheet" href="./resources/style.css"/>
</head>
<body>
    <?php
        echo $page->showPage();
    ?>
</body>
</html>