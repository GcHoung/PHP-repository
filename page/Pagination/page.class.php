<?php
namespace Pagination;
/**
 * @author:GcHoung
 * Date: 2015/12/8
 * Time: 9:19
 * page class
 */

class Page {

    /**
     * @var int
     * 总记录数,需要分页显示的总数据条数
     */
    private $dataNum;

    /**
     * @var string
     * 查询语句
     */
    private $str;

    /**
     * @var string
     * sql语句limit条件
     */
    private $limit;

    /**
     * @var int
     * 每一页的记录条数
     */
    private $pageSize;

    /**
     * @var int
     * 当前的页码
     */
    public $currentPage;

    /**
     * @var string
     * 基准url地址
     */
    private $url;

    /**
     * @var int
     * 总的页码数
     */
    private $pageNum;

    /**
     * @var int
     * 当前页码两边显示的偏移量
     */
    private $pageOffset;

    /**
     * @var boolean
     * 是否显示总页数和跳转按钮
     */
    private $showPagenum;

    /**
     * @var int
     * 分页类型
     * 1-->只显示上一页和下一页两个按钮
     * 2-->只显示数字页码
     * 3-->显示数字页码和上下页按钮
     * 4-->在3的基础上显示首页，尾页
     */
    private $pageType;

    /**
     *@var array 
     *分页按钮样式
     */
    private $styleArr = array(
        'currentBtnStyle'  => 'current-btn',
        'pageBtnStyle'     => 'page-btn',
        'offsetBtnStyle'   => 'offset-btn'
        );

    /**
     *构造函数，初始化参数的值
     * @param $str  查询sql语句
     * @param int $pageSize 每页显示的数据条数
     * @return void
     */
    function __construct ( $str, $pageSize = 4, $pageType = 1, $showPagenum = true )
    {
        $this->pageOffset  = 2;
        $this->showPagenum = $showPagenum;
        $this->pageType    = $pageType;
        $this->str         = $str;
        $this->pageSize    = $pageSize ;
        $this->dataNum     = $this->getDataNum();
        $this->pageNum     = ceil ( $this->dataNum / $this->pageSize );
        $this->url         = $this->setUrl();
        $this->currentPage = $this->getCurrentPage();
    }

    /**
     *设置分页类按钮样式，通过传入自定义的样式的类名来设定
     *@param $btn 按钮类名键名，currentBtnStyle：当前页按钮，pageBtnStyle：普通数字页码按钮， offsetBtnStyle：偏移按钮 
     *@param $val 按钮类名 默认：current-btn,page-btn,offset-btn
     *@return void
     */
    public function setBtnStyle ( $btn, $val )
    {
        $_keyArr = array();
        $_valArr = array();
        foreach ( $this->styleArr as $key => $value ) {
            $_keyArr[] = $key;
            $_valArr[] = $value;
        }

        //如果要设置的样式存在，赋予该样式新的类名
        if ( in_array ( $btn, $_keyArr ))
        {
            $this->styleArr[$btn] = $val;
        }
    }

    /**
     *设置基准url
     * @return string url地址
     */
    private function setUrl ()
    {

        $_url = $_SERVER['REQUEST_URI'].'?';
        $_par = parse_url ( $_url ) ;
        if ( isset ( $_par['query'] ) ) {
            parse_str ( $_par['query'], $_query );
            unset ( $_query['currentPage'] ) ;
            unset ( $_query['datanum'] ) ;
            $_url = $_par['path'].'?'.http_build_query ( $_query );
        }
        return $_url;
    }

    /**
     *得到当前页码
     * @return int 返回当前页码
     */
    private function getCurrentPage ()
    {
        if ( !empty ( $_GET['currentPage'] ))
        {
            $c_page = intval ( $_GET['currentPage'] );
            if ( $c_page < 1 ) return 1;
            if ( $c_page > $this->pageNum ) return $this->pageNum;
            return $c_page;
        }
        return 1;

    }

    /**
     *输出分页按钮
     * @return string 输出分页
     */
    public function showPage ()
    {
        $pagelist = $this->combinePageList ();
        return $pagelist;

    }

    /**
     *组合分页按钮
     * @return string 组合分页
     */
    private function combinePageList ()
    {
        $pageist = "";

        //显示总页数和跳转功能，需要js函数作为响应
        if ( $this->showPagenum )
        {
            $_showPageNum = "共 $this->pageNum 页 到第<input type='number' name='jumppage'min='1' max='{$this->pageNum}'> <input type='button' value='确定' onclick=''>";
        } else {
            $_showPageNum = '';
        }

        switch ( $this->pageType )
        {
            //只显示偏移按钮
            case 1:
                $pageist .= $this->lastPage().$this->nextPage().$_showPageNum;
                break;
            //只显示数字按钮
            case 2:
                $pageist .= $this->numberPage().$_showPageNum;
                break;
            //显示1和2
            case 3:
                $pageist .= $this->lastPage().$this->numberPage().$this->nextPage().$_showPageNum;
                break;
            //显示3和首页尾页按钮
            case 4:
                $pageist .= $this->firstPage().$this->lastPage().$this->numberPage().$this->nextPage().$this->endPage().$_showPageNum;
        }
        return $pageist;
    }

    /**
     *得到上一页按钮
     * @return string|void 返回上一页
     */
    private function lastPage ()
    {   
        $l_page = $this->currentPage - 1;  //上一页页码

        //当前页码为第一页时，不返回‘上一页’按钮
        if ( $this->currentPage == 1 )
        {
            //return "<a class='offset-btn' href='javascript:;'>上一页</a>";
            return ;
        }else{
            return "<a class='{$this->styleArr['offsetBtnStyle']}' href='{$this->url}&datanum={$this->dataNum}&currentPage=$l_page'>上一页</a>";
        }
    }

    /**
     *得到下一页按钮
     * @return string|void 返回下一页
     */
    private function nextPage ()
    {   
        $n_page = $this->currentPage + 1;  //下一页页码

        //当前页码为最后一页时，不显示‘下一页’按钮
        if ( $this->currentPage == $this->pageNum )
        {
            return ;
            //return "<a class='offset-btn' href='javascript:;'>下一页</a>";
        }else{
            return "<a class='{$this->styleArr['offsetBtnStyle']}' href='{$this->url}&datanum={$this->dataNum}&currentPage=$n_page'>下一页</a>";
        }
    }

    /**
     *得到首页按钮
     * @return string|void 返回首页
     */
    private function firstPage ()
    {
        if ( $this->currentPage == 1 ){
            return ;
        }else{
            return "<a class='{$this->styleArr['pageBtnStyle']}' href='{$this->url}&datanum={$this->dataNum}&currentPage=1'>首页</a>";
        }
    }

    /**
     *得到尾页按钮
     * @return string|void 返回尾页
     */
    private function endPage ()
    {
        if ( $this->currentPage == $this->pageNum ){
            return ;
        }else{
            return "<a class='{$this->styleArr['pageBtnStyle']}' href='{$this->url}&datanum={$this->dataNum}&currentPage={$this->pageNum}'>尾页</a>";
        }
    }

    /**
     *得到数字页码按钮
     * @return string 数字页码
     */
    private function numberPage ()
    {
        $pageist = '';

        //左边偏移
        for ( $i = $this->pageOffset; $i > 0; $i-- )
        {
            $c_page = $this->currentPage - $i;
            if ( $c_page < 1 ) continue;
            $pageist .= "<a class='{$this->styleArr['pageBtnStyle']}' href='{$this->url}&datanum={$this->dataNum}&currentPage=$c_page'>$c_page</a>";
        }
        //当前按钮
        $pageist .= "<a class='{$this->styleArr['currentBtnStyle']}' href='{$this->url}&datanum={$this->dataNum}&currentPage={$this->currentPage}'>{$this->currentPage}</a>";

        //右边偏移
        for ( $j = 1; $j <= $this->pageOffset; $j++ )
        {
            $c_page = $this->currentPage + $j;
            if ( $c_page > $this->pageNum ) break ;
            $pageist .= "<a class='{$this->styleArr['pageBtnStyle']}' href='{$this->url}&datanum={$this->dataNum}&currentPage=$c_page'>$c_page</a>";
        }

        return $pageist;
    }

    /**
     *得到总的记录条数
     * @return int 总的记录条数
     */
    private function getDataNum ()
    {

        if ( !empty ( $_GET['datanum'] ))
        {
            $_dataNum = intval ( $_GET['datanum'] );
            return $_dataNum;
        } else {
            //根据$str获取数据总条数，这里有数据库操作
            //测试直接返回
            return 100 ;
        }

    }
}
