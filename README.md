##PHP-repository PHP代码库 常用代码
---

###1. page

> 描述：php分页类

####使用
```
require "./Pagination/page.class.php";

//@param sql语句
//@param 每页的数据条数
//@param 分页显示：1.只显示上一页和下一页按钮，2.只显示数字页码按钮，3.显示1，2两种按钮，4.显示1，2和首页，尾页按钮
//@param 是否显示总页数和跳转功能
 
$page = new Pagination\Page($str,10,1,true);

```